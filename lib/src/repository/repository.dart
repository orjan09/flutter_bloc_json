import 'package:http/http.dart' show get;
import '../models/model.dart';
import 'dart:convert';

class DataServices {
  final _baseUrl = 'jsonplaceholder.typicode.com';

  Future<List<Model>> fetchImages() async {
    try {
      final uri = Uri.https(_baseUrl, '/photos');
      final response = await get(uri);
      final json = jsonDecode(response.body) as List;
      final images = json.map((postJson) => Model.fromJson(postJson)).toList();
      return images;
    } catch (e) {
      throw e;
    }
  }
}
