import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bloc_json/src/bloc/bloc.dart';
import 'package:flutter_bloc_json/src/models/model.dart';

class AppView extends StatelessWidget {
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Images'),
      ),
      body: BlocBuilder<PostsBloc, PostsState>(
        builder: (context, state) {
          if (state is LoadingPostsState) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (state is LoadedPostsState) {
            return RefreshIndicator(
              onRefresh: () async {
                return BlocProvider.of<PostsBloc>(context)
                    .add(PullToRefreshEvent());
              },
              child: ListView.builder(
                itemCount: 10,
                itemBuilder: (context, index) {
                  return Card(
                    child: ListTile(
                      title: buildImage(state.images![index]),
                    ),
                  );
                },
              ),
            );
          } else if (state is FailedToLoadPostsState) {
            return Center(
              child: Text('Error ocurreed: ${state.error}'),
            );
          } else {
            return Container();
          }
        },
      ),
    );
  }

  Widget buildImage(Model model) {
    return Container(
      //defines spacing in an element
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey),
      ),
      padding: EdgeInsets.all(20.0),
      margin: EdgeInsets.all(20.0),
      child: Column(
        // <Widget> not required
        children: <Widget>[
          Padding(
              child: Image.network(model.url!),
              padding: EdgeInsets.only(
                bottom: 8.0,
              )),
          Text(model.title!),
        ],
      ),
    );
  }
}
