class Model {
  final int? id;
  final String? title;
  final String? url;

  Model({this.id, this.title, this.url});

  factory Model.fromJson(Map<String, dynamic> jsonSource) {
    return Model(
      id: jsonSource['id'],
      title: jsonSource['title'],
      url: jsonSource['url'],
    );
  }
}
