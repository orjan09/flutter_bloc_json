import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bloc/bloc.dart';
import '../repository/repository.dart';
import '../models/model.dart';
part 'event_state.dart';

class PostsBloc extends Bloc<PostsEvent, PostsState> {
  final _data = DataServices();
  PostsBloc() : super(LoadingPostsState());

  @override
  Stream<PostsState> mapEventToState(PostsEvent event) async* {
    if (event is LoadPostsEvent || event is PullToRefreshEvent) {
      yield LoadingPostsState();
      try {
        final images = await _data.fetchImages();
        yield LoadedPostsState(images: images);
      } catch (e) {
        yield FailedToLoadPostsState(error: e);
      }
    }
  }
}
