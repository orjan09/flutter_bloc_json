part of 'bloc.dart';

abstract class PostsEvent {}

class LoadPostsEvent extends PostsEvent {}

class PullToRefreshEvent extends PostsEvent {}

abstract class PostsState {}

class LoadingPostsState extends PostsState {}

class LoadedPostsState extends PostsState {
  List<Model>? images;

  LoadedPostsState({this.images});
}

class FailedToLoadPostsState extends PostsState {
  dynamic error;
  FailedToLoadPostsState({this.error});
}
