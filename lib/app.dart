import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bloc_json/src/bloc/bloc.dart';
import 'package:flutter_bloc_json/src/widgets/ui.dart';

class MyApp extends StatefulWidget {
  State<StatefulWidget> createState() {
    return _AppState();
  }
}

class _AppState extends State<MyApp> {
  Widget build(BuildContext context) {
    return MaterialApp(
      home: BlocProvider<PostsBloc>(
        create: (_) {
          return PostsBloc()..add(LoadPostsEvent());
        },
        child: AppView(),
      ),
    );
  }
}
